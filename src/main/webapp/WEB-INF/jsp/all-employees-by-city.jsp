<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="webjars/jquery/3.6.0/dist/jquery.min.js"></script>
<link rel="stylesheet" href="webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>
<script type="text/javascript" src="webjars/bootstrap/4.6.0/css/bootstrap.min.js"></script>
<script type="text/javascript" src="webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="webjars/jquery/1.12.0/jquery.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	<h3 style="text-align:center">All Employee</h3>
	<table>
		<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Age</th>
				<th>Salary</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${employees }" var="employee">
				<tr>
					<td>${employee.employeeId}</td>
					<td>${employee.employeeName}</td>
					<td>${employee.age}</td>
					<td>${employee.salary}</td>
				</tr>
			</c:forEach>
			<tr>
				<td><a href="save"><img src="webjars/bootstrap-icons/1.3.0/icons/arrow-left-circle.svg" alt="arrow-left-circle" class="left-arrow-img">back</a></td>
			</tr>
		</tbody>
	</table>
</body>
</html>