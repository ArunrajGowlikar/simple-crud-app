<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Save Employee</title>
<script type="text/javascript" src="webjars/jquery/3.6.0/dist/jquery.min.js"></script>
	<link rel="stylesheet" href="webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>
	<script type="text/javascript" src="webjars/bootstrap/4.6.0/css/bootstrap.min.js"></script>
	<script type="text/javascript" src="webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="webjars/jquery/1.12.0/jquery.min.js"></script>
	<style>
		html{
			box-type:border-box;
			height:100vh;
		}
		body{
			box-type:border-box;
			height:100vh;
		}
		.title{
			text-align: center;
		}
		.container{
			height:100vh;
			display: flex;
			justify-content: center;
			/*align-items: center;*/
		}
		.right-arrow-img{
			width: 1.5rem;
			height: 1.5rem;
			margin-right: 4px;
		}
		.red{
			color: red;
		}
	</style>
</head>
<body>
	<c:if test="${updated eq 'saved' }">
		<h2 style="color:green">Employee Saved Successfully</h2>
	</c:if>
	<c:if test="${updated eq 'updated' }">
		<h2 style="color:blue">Employee Updated Successfully</h2>
	</c:if>
	<c:if test="${err ne null }">
		<h2 style="color:red">${err}</h2>
	</c:if>
	<h2 class="title">Save Employee</h2>
	<div class="container">
	<form:form action="save-emp" modelAttribute="employeeModal" method="POST" enctype="multipart/form-data" >
		<%-- <form:errors path="employeeModal.*" cssClass="red" /> --%>
		<%-- <spring:message code="NotEmpty.employeeName" /> --%>
		<table>
			<%-- <c:if test="employee.id ne null"> --%>
				<tr>
					<td><!--Employee Id  --></td>
					<td><form:hidden path="employeeId" /></td>
					<td><%-- <form:errors path="employeeId"/> --%> </td>	
				</tr>
			<%-- </c:if> --%>
			<tr>
				<td>Name</td>
				<td><form:input cssClass="form-control" path="employeeName"/></td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="employeeName" cssClass="red" /> </td>
			</tr>
			<tr>
				<td>Age</td>
				<td><form:input cssClass="form-control" path="age" type="number"/></td>
					
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="age" cssClass="red"/> </td>	
			</tr>
			<tr>
				<td>Salary</td>
				<td><form:input cssClass="form-control" path="salary" type="number"/></td>
					
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="salary" cssClass="red"/> </td>	
			</tr>
			<tr>
				<td>Department</td>
				<td>
					<form:select cssStyle="form-select" path="department" id="selectItem">
						<form:option value="-1">Select</form:option>
						<c:forEach items="${departments}" var="dep">
							<form:option value="${dep.key}">${dep.value}</form:option>
						</c:forEach>
					</form:select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><form:errors path="department" cssClass="red"/> </td>
			</tr>
			<tr>
				<td>Upload Image</td>
				<td><form:input type="file" path="image" accept="image/*"/> </td>
			</tr>
			<!-- add Address fields here -->
			<tr>
				<td>Address Information</td>
				<td></td>
			</tr>
			 
			<c:forEach items="${employeeModal.addresses}" var="address" varStatus="vs">
				<tr>
					<td></td>
					<td><form:hidden path="addresses[${vs.index}].addressNo" cssClass="form-control"/> 
						<%-- <input type="text" name="addresses[${vs.index}].addressNo" /> --%>
					</td>
				</tr>
				<tr>
					<td>Address Line1</td>
					<td><form:input path="addresses[${vs.index}].line1" cssClass="form-control"/></td>
				</tr>
				<tr>
					<td>Address Line2</td>
					<td><form:input path="addresses[${vs.index}].line2" cssClass="form-control"/> </td>
				</tr>
				<tr>
					<td>Zip code</td>
					<td><form:input path="addresses[${vs.index}].zip" cssClass="form-control"/></td>
				</tr>
				<tr>
					<td>City</td>
					<td>
					<form:select cssStyle="form-select" path="addresses[${vs.index}].city" >
						<form:option value="-1">Select</form:option>
						<c:forEach items="${cities}" var="city">
							<form:option value="${city.key}">${city.value}</form:option>
						</c:forEach>
					</form:select>
					</td>
				</tr>
			</c:forEach>
			 
			<!-- Address fields ended here -->
			<tr>
				<td></td>
				<td>
				<div class="btn-group">
					<input type="reset" class="btn btn-secondary text-right" value="reset" onclick="resetFields()">
					<input type="submit" class="btn btn-info text-left" value="submit">
				</div>
				</td>
			</tr>
		</table>
		<div>
			<a href="view-all"><img src="webjars/bootstrap-icons/1.3.0/icons/arrow-right-circle.svg" alt="arrow-right-circle" class="right-arrow-img">Show All Employees</a>
			<a href="employee-by-city"><img src="webjars/bootstrap-icons/1.3.0/icons/arrow-right-circle.svg" alt="arrow-right-circle" class="right-arrow-img">Employees by city</a>
		</div>
		<div><a href="save-excel"><img src="webjars/bootstrap-icons/1.3.0/icons/file-spreadsheet.svg" alt="spreadsheet" class="right-arrow-img"/>Upload Excel</a></div>
	</form:form>
	</div>
	<script type="text/javascript">
		/*function resetFields(){
			var selectItem = document.getElementById("selectItem");
			selectItem.selectedIndex = 0;
			file-spreadsheet.svg
		}*/
	</script>
</body>
</html>