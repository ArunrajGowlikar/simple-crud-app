<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <script type="text/javascript" src="webjars/jquery/3.6.0/dist/jquery.min.js"></script>
	<link rel="stylesheet" href="webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>
	<script type="text/javascript" src="webjars/bootstrap/4.6.0/css/bootstrap.min.js"></script>
	<script type="text/javascript" src="webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="webjars/jquery/1.12.0/jquery.min.js"></script> -->
	<script type="text/javascript" src="webjars/jquery/3.6.0/dist/jquery.min.js"></script>
	<link rel="stylesheet" href="webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>
	<script type="text/javascript" src="webjars/bootstrap/4.6.0/css/bootstrap.min.js"></script>
	<script type="text/javascript" src="webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="webjars/jquery/1.12.0/jquery.min.js"></script>
	<script type="text/javascript" src="webjars/datatables/1.9.4/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="webjars/datatables/1.9.4/media/css/jquery.dataTables.css"></script>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
	</script>
<title>Insert title here</title>
</head>
<body>
	<h3 style="color:red">
		<c:if test="${err ne null}">${err}</c:if>
	</h3>
	<form:form action="save-excel" modelAttribute="saveExcel" method="POST" enctype="multipart/form-data">
		<div><form:input path="file" type="file" accept=".xls, .xlsx"/> </div>
		<input type="submit" class="btn btn-info text-left" value="submit">
	</form:form>
	<c:if test="${failList ne null}">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Age</th>
				<th>Salary</th>
				<th>Department</th>
				<th>AddressLine1</th>
				<th>AddressLine2</th>
				<th>Zip</th>
				<th>City</th>
				<th>AddressLine1</th>
				<th>AddressLine2</th>
				<th>Zip</th>
				<th>City</th>
				<th>Status</th>
				<th>Errors</th>
			</tr>
		<thead>
		<tbody>
			<c:forEach items="${failList}" var="emp">
			<tr>
				<td>${emp.employeeName}</td>
				<td>${emp.age}</td>
				<td>${emp.salary }</td>
				<%-- <c:forEach items="${departments}" var="dep"> --%>
				<c:choose>
					<c:when test="${emp.department ne null}">
						<c:forEach items="${departments}" var="dep">
							<c:if test="${dep.departmentNo eq  emp.department}">
								<td>${ dep.departmentName}</td>
							</c:if>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<td>null</td>
					</c:otherwise>
				</c:choose>
					<%-- <c:if test="">
						
					</c:if> --%>
				<%-- </c:forEach> --%>
				<c:forEach items="${emp.addresses}" var="add">
					<td>${add.line1}</td>
					<td>${add.line2}</td>
					<td>${add.zip}</td>
					<c:forEach items="${cities}" var="city">
						<c:if test="${city.cityId eq add.city }">
							<td>${city.cityName}</td>
						</c:if>
					</c:forEach>
				</c:forEach>
				<td>${emp.status}</td>
				<td>${emp.errors}</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
	</c:if>
</body>
</html>