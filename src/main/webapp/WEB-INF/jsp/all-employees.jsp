<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Employees</title>
	<script type="text/javascript" src="webjars/jquery/3.6.0/dist/jquery.min.js"></script>
	<link rel="stylesheet" href="webjars/bootstrap/4.6.0/css/bootstrap.min.css"/>
	<script type="text/javascript" src="webjars/bootstrap/4.6.0/css/bootstrap.min.js"></script>
	<script type="text/javascript" src="webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="webjars/jquery/1.12.0/jquery.min.js"></script>
	<script type="text/javascript" src="webjars/datatables/1.9.4/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="webjars/datatables/1.9.4/media/css/jquery.dataTables.css"></script>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
	
		function confirmDelete(){
			return confirm("are you sure you want to delete?");
		}
	</script>
	<style>
		.left-arrow-img{
			width: 1.3rem;
			height: 1.3rem;
			margin-right: 4px;
		}
	</style>
</head>
<body>
<h2 style="text-align:center">All Employees</h2>
<!-- <a href="save"><img src="webjars/bootstrap-icons/1.3.0/icons/arrow-left-circle.svg" alt="arrow-left-circle" class="left-arrow-img">Add Employee</a> -->
<c:if test="${err ne null }">
	<h2 style="color:red">${err }</h2>
</c:if>
<c:if test="${status ne null }">
	<h2 style="color:green">${status }</h2>
</c:if>
<div class="container">
<a href="save"><img src="webjars/bootstrap-icons/1.3.0/icons/arrow-left-circle.svg" alt="arrow-left-circle" class="left-arrow-img">Add Employee</a>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Age</th>
				<th>Salary</th>
				<th>Department</th>
				<th>Action</th>
			</tr>
		<thead>
		<tbody>
		<c:forEach items="${employees}" var="employee">
			<tr>
				<td>${employee.employeeId}</td>
				<td>${employee.employeeName}</td>
				<td>${employee.age}</td>
				<td>${employee.salary}</td>
				
				<c:forEach items="${departments}" var="dep">
					<c:if test="${employee.department eq dep.key}">
						<td>${dep.value }</td>
					</c:if>
				</c:forEach>
				<td><a href="update-employee?id=${employee.employeeId}" class="btn btn-primary">update</a> 
					<a href="delete-employee?id=${employee.employeeId }" class="btn btn-danger" onclick="return confirmDelete()">delete</a>
					<a href="generate-pdf/${employee.employeeId}" class="btn btn-primary">PDF</a>
					</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</div>
</body>
</html>