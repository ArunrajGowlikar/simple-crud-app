<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form:form action="/employee-by-city" modelAttribute="cityModel" method="POST">
		<table>
		<tr>
			<td>Select to download</td>
			<td><form:checkbox path="download" value="download"/> </td>
		</tr>
			<tr>
				<td>Select City</td>
				<td>
					<form:select path="cityId" onchange="this.form.submit()" >
						<form:option value="-1" >Select</form:option>
						<c:forEach items="${cities}" var="city">
							<form:option value="${city.key}">${city.value}</form:option>
						</c:forEach>
					</form:select>
				</td>
			</tr>
		</table>
	</form:form>
</body>
</html>