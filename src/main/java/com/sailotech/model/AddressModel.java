package com.sailotech.model;

public class AddressModel {
	
	private Integer addressNo;
	private String line1;
	private String line2;
	private Integer zip;
	private Integer city;

	public Integer getAddressNo() {
		return addressNo;
	}

	public void setAddressNo(Integer addressNo) {
		this.addressNo = addressNo;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public Integer getZip() {
		return zip;
	}

	public void setZip(Integer zip) {
		this.zip = zip;
	}

	public Integer getCity() {
		return city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "AddressModel [addressNo=" + addressNo + ", line1=" + line1 + ", line2=" + line2 + ", zip=" + zip
				+ ", city=" + city + "]";
	}

}
