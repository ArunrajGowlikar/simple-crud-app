package com.sailotech.model;

public class DepartmentModel {

	private int departmentNo;
	private String departmentName;

	public int getDepartmentNo() {
		return departmentNo;
	}

	public void setDepartmentNo(int departmentNo) {
		this.departmentNo = departmentNo;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	@Override
	public String toString() {
		return "DepartmentModel [departmentNo=" + departmentNo + ", departmentName=" + departmentName + "]";
	}

}
