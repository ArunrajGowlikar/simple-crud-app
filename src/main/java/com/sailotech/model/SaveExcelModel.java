package com.sailotech.model;

import org.springframework.web.multipart.MultipartFile;

public class SaveExcelModel {
	
	private String checkbox;
	private MultipartFile file;

	public String getCheckbox() {
		return checkbox;
	}

	public void setCheckbox(String checkbox) {
		this.checkbox = checkbox;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return "SaveExcelModel [checkbox=" + checkbox + ", file=" + file + "]";
	}

}
