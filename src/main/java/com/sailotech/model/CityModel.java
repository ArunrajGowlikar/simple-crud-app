package com.sailotech.model;

public class CityModel {

	private String cityName;
	private Integer cityId;
	private String download;

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getDownload() {
		return download;
	}

	public void setDownload(String download) {
		this.download = download;
	}

	@Override
	public String toString() {
		return "CityModel [cityName=" + cityName + ", cityId=" + cityId + ", download=" + download + "]";
	}

}
