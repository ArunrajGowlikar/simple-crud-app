package com.sailotech.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.sailotech.model.EmployeeModel;

@Component(value = "employeeFormValidator")
public class EmployeeFormValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		if (clazz.isAssignableFrom(EmployeeModel.class)) {
			return true;
		}
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		EmployeeModel emp = (EmployeeModel) target;
		/*if(emp == null){
			errors.rejectValue("employeeName", "employeeName.notEmpty");
			errors.rejectValue("age", "age.invalid");
			errors.rejectValue("salary", "salary.invalid");
			errors.rejectValue("department", "department.invalid");
			System.out.println("============================ in == null ============================");
		}*/
		if (emp != null) {
			if (emp.getEmployeeName() == null || emp.getEmployeeName().trim().length() <= 0) {
				errors.rejectValue("employeeName", "employeeName.notEmpty");
				// errors.rejectValue("employeeName", "NotEmpty.employeeName",
				// "employee name cannot be empty");
			}
			/*
			 * double salary = emp.getSalary(); int age = emp.getAge(); int
			 * department = emp.getDepartment();
			 */

			if (emp.getAge() == null || emp.getAge() <= 0 || emp.getAge() > 100) {
				errors.rejectValue("age", "age.invalid");
			}

			/*
			 * if(age<1 && age > 100){ errors.rejectValue("age", "age.invalid");
			 * }
			 */

			if (emp.getSalary() == null || emp.getSalary() <= 0 || emp.getSalary() > 999999) {
				errors.rejectValue("salary", "salary.invalid");
			}

			/*
			 * if(salary<1 && salary >999999){ errors.rejectValue("salary",
			 * "salary.invalid"); }
			 */

			if (emp.getDepartment() == null || emp.getDepartment() <= -1) {
				errors.rejectValue("department", "department.invalid");
			}
			/*
			 * if(department <=-1){ errors.rejectValue("department",
			 * "department.invalid"); }
			 */

		}

	}

}
