package com.sailotech.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sailotech.entity.Address;
import com.sailotech.entity.Employee;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>{

	//@Query("select a.employee from Address a inner join a.employee e on (e.employee.employeeId=a.employee.employeeId) where a.city.cityId=?1 and e.isDelInd='n'")
	@Query("select distinct a.employee from Address a inner join a.employee e on e.employeeId=a.employee.employeeId where a.city.cityId=?1 and e.isDelInd='n'")
	public List<Employee> findAllEmployeesByCityId(Integer cityId);
}
