package com.sailotech.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sailotech.entity.Employee;
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
	@Query("select e from Employee e where e.employeeId=?1 and e.isDelInd='n'")
	public Employee findByEmployeeId(Integer id);
	
	@Query("select e from Employee e where e.isDelInd='n'")
	public List<Employee> findAllUnDeletedEmployees();
	
	
	@Query("update Employee e set e.isDelInd='y' where e.employeeId=?1")
	@Modifying
	public void deleteEmployee(Integer id);
}
