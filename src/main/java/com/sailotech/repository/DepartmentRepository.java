package com.sailotech.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sailotech.entity.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer>{

	@Query("select d from Department d where d.isDelInd='n'")
	public List<Department> findAllUnDeletedDepartments();
	
	@Query("select d from Department d where d.departmentNo=?1 and d.isDelInd='n'")
	public Department findDepartmentById(Integer id);
}
