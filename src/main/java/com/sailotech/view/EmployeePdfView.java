package com.sailotech.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import com.sailotech.model.AddressModel;
import com.sailotech.model.CityModel;
import com.sailotech.model.DepartmentModel;
import com.sailotech.model.EmployeeModel;
import com.sailotech.service.CityManagementService;
import com.sailotech.service.DepartmentManagementService;

@Component(value="pdfView")
@Qualifier(value="pdfView")
public class EmployeePdfView extends AbstractPdfView{

	private DepartmentManagementService depService;
	private CityManagementService cityMgmt;
	
	
	@Autowired
	public EmployeePdfView(DepartmentManagementService depService,CityManagementService cityMgmt) {
		super();
		this.depService = depService;
		this.cityMgmt=cityMgmt;
	}



	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

			EmployeeModel employee = (EmployeeModel) model.get("employee");
			List<DepartmentModel> allDep = depService.getAllDepartments();
			List<AddressModel> addresses = employee.getAddresses();
			//PdfTable table = new PdfTable(2);
			Table table = new Table(2);
			table.addCell("Name");
			table.addCell(employee.getEmployeeName());
			table.addCell("Age");
			table.addCell(String.valueOf(employee.getAge()));
			table.addCell("Salary");
			table.addCell(String.valueOf(employee.getSalary()));
			
			Table add1 = new Table(2);
			Table add2 = new Table(2);
			
			table.addCell("Department");
			Integer depId = employee.getDepartment();
			for(DepartmentModel dep : allDep){
				if(depId == dep.getDepartmentNo()){
					table.addCell(dep.getDepartmentName());
				}
			}
			table.addCell("Address 1");
			table.addCell("Address 2");
			
			
			
			
			List<CityModel> allCities = cityMgmt.getAllCities();
			int count=0;
			for(AddressModel a : addresses){
				if(count == 0){
					add1.addCell("Line1");
					add1.addCell(a.getLine1());
					add1.addCell("Line2");
					add1.addCell(a.getLine2());
					add1.addCell("Zip");
					add1.addCell(String.valueOf(a.getZip()));
					Integer cityId = a.getCity();
					
					for(CityModel c : allCities){
						if(c.getCityId() == cityId){
							add1.addCell(c.getCityName());
						}
					}
					
				}else{
					add2.addCell("Line1");
					add2.addCell(a.getLine1());
					add2.addCell("Line2");
					add2.addCell(a.getLine2());
					add2.addCell("Zip");
					add2.addCell(String.valueOf(a.getZip()));
					Integer cityId = a.getCity();
					for(CityModel c : allCities){
						if(c.getCityId() == cityId){
							add2.addCell(c.getCityName());
						}
					}
				}
				count++;
			}
			Cell cell1 = new Cell();
			cell1.add(add1);
			Cell cell2 = new Cell();
			cell2.add(add1);
			
			table.addCell(cell1);
			table.addCell(cell2);
			document.add(table);
			
	}

}
