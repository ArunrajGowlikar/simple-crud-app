package com.sailotech.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sailotech.model.AddressModel;
import com.sailotech.model.CityModel;
import com.sailotech.model.DepartmentModel;
import com.sailotech.model.EmployeeModel;
import com.sailotech.service.CityManagementService;
import com.sailotech.service.DepartmentManagementService;
import com.sailotech.service.EmployeeManagementService;
import com.sailotech.validation.EmployeeFormValidator;

@Controller
//@RequestMapping("/save")
public class EmployeeSaveController {
	
	private EmployeeManagementService employeeManagementService;
	private DepartmentManagementService departmentManagementService;
	private EmployeeFormValidator employeeFormVAlidator;
	private CityManagementService cityManagementService;
	
	
	@Autowired
	public EmployeeSaveController(EmployeeManagementService employeeManagementService,DepartmentManagementService departmentManagementService,EmployeeFormValidator employeeFormVAlidator, CityManagementService cityManagementService) {
		super();
		this.employeeManagementService = employeeManagementService;
		this.departmentManagementService = departmentManagementService;
		this.employeeFormVAlidator=employeeFormVAlidator;
		this.cityManagementService=cityManagementService;
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		binder.addValidators(employeeFormVAlidator);
	}

	@RequestMapping(method={RequestMethod.GET}, path={"/","/save"})
	public String showSavePage(Model model){
		Map<Integer,String> map = new HashMap<>();
		List<DepartmentModel> departments = departmentManagementService.getAllDepartments();
		for(DepartmentModel department : departments){
			map.put(department.getDepartmentNo(), department.getDepartmentName());
		}
		List<CityModel> allCities = cityManagementService.getAllCities();
		Map<Integer,String> citiesMap = new HashMap<>();
		for(CityModel c : allCities){
			citiesMap.put(c.getCityId(), c.getCityName());
		}
		model.addAttribute("cities", citiesMap);
		model.addAttribute("departments", map);
		AddressModel m1 = new AddressModel();
		AddressModel m2 = new AddressModel();
		List<AddressModel> addModelList = new ArrayList<>();
		addModelList.add(m1);
		addModelList.add(m2);
		EmployeeModel emp = new EmployeeModel();
		emp.setAddresses(addModelList);
		model.addAttribute("employeeModal", emp);
		return "save-employee";
	}

	@RequestMapping(method={RequestMethod.POST}, path={"/save-emp"})
	public String getMessage(@ModelAttribute("employeeModal") @Valid EmployeeModel empModel,BindingResult errors,RedirectAttributes model, Model modelMap){
			if(errors.hasErrors()){
				Map<Integer,String> map = new HashMap<>();
				List<DepartmentModel> departments = departmentManagementService.getAllDepartments();
				for(DepartmentModel department : departments){
					map.put(department.getDepartmentNo(), department.getDepartmentName());
				}
				modelMap.addAttribute("departments", map);
				return "save-employee";
			}
		try{
			MultipartFile file = empModel.getImage();
			String fileName = file.getOriginalFilename();
			if(fileName != null && !fileName.equals("")){
				File f = new File("f:\\images");
				if(!f.exists()){
					f.mkdir();
				}
				f= new File(f+"\\"+fileName);
				f.createNewFile();
				
				file.transferTo(f);
			}
			String value = employeeManagementService.saveEmployee(empModel);
			if(value.equals("saved")){
				model.addFlashAttribute("updated", "saved");
			}else{
				model.addFlashAttribute("updated", "updated");
			}
		}catch(Exception e){
			model.addFlashAttribute("err", "Something went wrong");
		}
		return "redirect:/save";
	}
	
}
