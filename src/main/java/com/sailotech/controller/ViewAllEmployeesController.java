package com.sailotech.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sailotech.model.CityModel;
import com.sailotech.model.DepartmentModel;
import com.sailotech.model.EmployeeModel;
import com.sailotech.service.CityManagementService;
import com.sailotech.service.DepartmentManagementService;
import com.sailotech.service.EmployeeManagementService;

@Controller
public class ViewAllEmployeesController {
	
	private EmployeeManagementService employeeManagementService;
	private DepartmentManagementService departmentManagementService;
	private CityManagementService cityManagementService;
	
	
	@Autowired
	public ViewAllEmployeesController(EmployeeManagementService employeeManagementService, DepartmentManagementService departmentManagementService,CityManagementService cityManagementService) {
		super();
		this.employeeManagementService = employeeManagementService;
		this.departmentManagementService = departmentManagementService;
		this.cityManagementService=cityManagementService; 
	}



	@RequestMapping(method={RequestMethod.GET},path={"/view-all"})
	public String viewAllContactsPage(Model model){
		List<EmployeeModel> allEmployees = employeeManagementService.getAllEmployees();
		
		List<DepartmentModel> departments = departmentManagementService.getAllDepartments();
		Map<Integer,String> map = convertDepartmentsToMap(departments);
		model.addAttribute("departments", map);
		model.addAttribute("employees", allEmployees);
		return "all-employees";
	}
	
	@RequestMapping(method={RequestMethod.GET},path={"/update-employee"})
	public String updateEmployeeById(@RequestParam("id")int id ,/*,@ModelAttribute("employeeModal") EmployeeModel empModel,*/Model model){
		/*EmployeeModel employee = employeeManagementService.findEmployeeById(id);
		BeanUtils.copyProperties(empModel, employee, "department");*/
		/*employeeManagementService.saveEmployee(empModel);*/
		EmployeeModel employee = employeeManagementService.findEmployeeById(id);
		List<CityModel> allCities = cityManagementService.getAllCities();
		Map<Integer,String> citiesMap = new HashMap<>();
		for(CityModel c : allCities){
			citiesMap.put(c.getCityId(), c.getCityName());
		}
		model.addAttribute("cities", citiesMap);
		List<DepartmentModel> departments = departmentManagementService.getAllDepartments();
		Map<Integer,String> map = convertDepartmentsToMap(departments);
		model.addAttribute("departments", map);
		model.addAttribute("employeeModal", employee);
		//model.addAttribute("employeeModal",employee);
		return "/save-employee";
	}
	
	@RequestMapping(method={RequestMethod.GET},path={"/delete-employee"})
	public String deleteEmployee(@RequestParam("id")int id, RedirectAttributes model){
		try{
			employeeManagementService.deleteEmployeeById(id);
			model.addFlashAttribute("status","Employee deleted succesfully");
		}catch(IllegalArgumentException e){
			model.addFlashAttribute("err","failed to delete Employee");
		}
		return "redirect:/view-all";
	}
	
	public Map<Integer,String> convertDepartmentsToMap(List<DepartmentModel> departments){
		Map<Integer,String> map = new HashMap<>();
		for(DepartmentModel department : departments){
			map.put(department.getDepartmentNo(), department.getDepartmentName());
		}
		return map;
	}
}
