package com.sailotech.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sailotech.model.AddressModel;
import com.sailotech.model.CityModel;
import com.sailotech.model.DepartmentModel;
import com.sailotech.model.EmployeeModel;
import com.sailotech.service.CityManagementService;
import com.sailotech.service.DepartmentManagementService;
import com.sailotech.service.EmployeeManagementService;

@Controller
public class EmployeeForCityController {
	
	private CityManagementService cityManagementService;
	private EmployeeManagementService employeeManagementService;
	private DepartmentManagementService departmentManagementService;

	@Autowired
	public EmployeeForCityController(CityManagementService cityManagementService,EmployeeManagementService employeeManagementService,DepartmentManagementService departmentManagementService) {
		super();
		this.cityManagementService = cityManagementService;
		this.employeeManagementService=employeeManagementService;
		this.departmentManagementService=departmentManagementService;
	}



	@RequestMapping(path={"/employee-by-city"},method={RequestMethod.GET})
	public String showEmployeeForCityPage(Model model){
		Map<Integer,String> cities = null;
		 cities = cityManagementService.getAllCities().stream().collect(Collectors.toMap(CityModel::getCityId, CityModel::getCityName));
		 model.addAttribute("cityModel", new CityModel());
		 model.addAttribute("cities", cities);
		return "employee-by-city";
	}
	
	@RequestMapping(path={"/employee-by-city"},method={RequestMethod.POST})
	public String showEmployeeForCity(@ModelAttribute("cityModel") CityModel cityModel, BindingResult errors, Model model,RedirectAttributes redirect, HttpServletResponse response){
		List<EmployeeModel> allEmployeesByCityId = employeeManagementService.getAllEmployeesByCityId(cityModel.getCityId());
		if(cityModel.getDownload() != null && cityModel.getDownload().equals("download")){
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=employees.xlsx");
			
			try{
				PrintWriter out = response.getWriter();
				XSSFWorkbook wb = new XSSFWorkbook();
				XSSFSheet sheet1 = wb.createSheet("sheet1");
				int rows=0;
				List<CityModel> allCities = cityManagementService.getAllCities();
				for(EmployeeModel e : allEmployeesByCityId){
					int cell = 0;
					XSSFRow createRow = sheet1.createRow(rows);
					//name
					XSSFCell createCell = createRow.createCell(cell);
					createCell.setCellValue(e.getEmployeeName());
					//age
					XSSFCell ageCell = createRow.createCell(++cell);
					ageCell.setCellValue(e.getAge());
					//salary
					XSSFCell salaryCell = createRow.createCell(++cell);
					salaryCell.setCellValue(e.getSalary());
					List<DepartmentModel> allDepartments = departmentManagementService.getAllDepartments();
					for(DepartmentModel d : allDepartments){
						if(e.getDepartment() == d.getDepartmentNo()){
							XSSFCell departmentCell = createRow.createCell(++cell);
							departmentCell.setCellValue(d.getDepartmentName());
						}
					}
					List<AddressModel> addresses = e.getAddresses();
					for(AddressModel a : addresses){
						//line1
						XSSFCell add1Cell = createRow.createCell(++cell);
						add1Cell.setCellValue(a.getLine1());
						//line2
						XSSFCell add1Line2Cell = createRow.createCell(++cell);
						add1Line2Cell.setCellValue(a.getLine2());
						//zip
						XSSFCell add1ZipCell = createRow.createCell(++cell);
						add1ZipCell.setCellValue(a.getZip());
						//cities
						Integer cityId = a.getCity();
						for(CityModel c : allCities){
							if(c.getCityId() == cityId){
								XSSFCell add1CityCell = createRow.createCell(++cell);
								add1CityCell.setCellValue(c.getCityName());
							}
						}
						
					}
					
					
					//cell +=1;
					rows +=1;
				}
				
				
				
				
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				wb.write(baos);
				InputStream is = new ByteArrayInputStream(baos.toByteArray());
				int i = is.read();
				while(i != -1){
					out.write(i);
					i = is.read();
				}
				out.flush();
				out.close();
				
			}catch(Exception e){
				redirect.addFlashAttribute("err", "Something went wrong");
				return "redirect:/employee-by-city";
			}
			//return "redirect:/employee-by-city";
			return "employee-by-city";
		}else{
		
		model.addAttribute("employees", allEmployeesByCityId);
		return "all-employees-by-city";
		}
	}
}
