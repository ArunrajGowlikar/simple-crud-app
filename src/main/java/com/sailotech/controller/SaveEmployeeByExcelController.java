package com.sailotech.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sailotech.model.AddressModel;
import com.sailotech.model.CityModel;
import com.sailotech.model.DepartmentModel;
import com.sailotech.model.EmployeeModel;
import com.sailotech.model.SaveExcelModel;
import com.sailotech.service.CityManagementService;
import com.sailotech.service.DepartmentManagementService;
import com.sailotech.service.EmployeeManagementService;

@Controller
public class SaveEmployeeByExcelController {
	
	private DepartmentManagementService departmentService;
	private CityManagementService cityService;
	private EmployeeManagementService employeeManagementService;
	
	
	@Autowired
	public SaveEmployeeByExcelController(DepartmentManagementService departmentService,
			CityManagementService cityService,
			EmployeeManagementService employeeManagementService) {
		super();
		this.departmentService = departmentService;
		this.cityService = cityService;
		this.employeeManagementService=employeeManagementService;
	}

	@RequestMapping(path={"/save-excel"},method={RequestMethod.GET})
	public String showSaveEmployeeByExcelPage(Model model){
		SaveExcelModel saveExcel = new SaveExcelModel();
		model.addAttribute("saveExcel", saveExcel);
		return "save-excel";
	}
	
	@RequestMapping(path={"/save-excel"},method={RequestMethod.POST})
	public String convertAndSaveEmployeesFromExcel(@ModelAttribute("saveExcel") SaveExcelModel saveExcelModel,BindingResult errors,RedirectAttributes redirect, Model model){
		MultipartFile file = saveExcelModel.getFile();
		List<EmployeeModel> employeeModelList = null;
		List<DepartmentModel> departments = departmentService.getAllDepartments();
		List<CityModel> allCities = cityService.getAllCities();
		try {
			XSSFWorkbook wb = new XSSFWorkbook(file.getInputStream());
			//HSSFWorkbook wb = new HSSFWorkbook(file.getInputStream());
			int numberOfSheets = wb.getNumberOfSheets();
			employeeModelList = new ArrayList<>();
			for(int i=0;i<numberOfSheets;i++){
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				while(rows.hasNext()){
					EmployeeModel eachEmpModel = new EmployeeModel();
					AddressModel ad1 = new AddressModel();
					AddressModel ad2 = new AddressModel();
					List<AddressModel> addresses = new ArrayList<>();
					Row row = rows.next();
					Iterator<Cell> cells = row.iterator();
					while(cells.hasNext()){
						Cell cell = cells.next();
						int index = cell.getColumnIndex();
						
						switch(index){
						/*case 0:
							eachEmpModel.setEmployeeId((int)cell.getNumericCellValue());
							break;*/
						case 0:
							eachEmpModel.setEmployeeName(cell.getStringCellValue());
							break;
						case 1:
							eachEmpModel.setAge((int)cell.getNumericCellValue());
							break;
						case 2:
							eachEmpModel.setSalary((double)cell.getNumericCellValue());
							break;
						case 3:
							//deapartments
							String depName = cell.getStringCellValue();
							
							for(DepartmentModel dep : departments){
								if(depName != null && depName.equals(dep.getDepartmentName())){
									eachEmpModel.setDepartment(dep.getDepartmentNo());
								}
							}
							break;
						case 4:
							ad1.setLine1(cell.getStringCellValue());
							break;
						case 5:
							ad1.setLine2(cell.getStringCellValue());
							break;
						case 6:
							ad1.setZip((int)cell.getNumericCellValue());
							break;
						case 7:
							//set city
							String cityName = cell.getStringCellValue();
							for(CityModel c : allCities){
								if(cityName != null && cityName.equals(c.getCityName())){
									ad1.setCity((int)c.getCityId());
								}
							}
							break;
						case 8:
							ad2.setLine1(cell.getStringCellValue());
							break;
						case 9:
							ad2.setLine2(cell.getStringCellValue());
							break;
						case 10:
							ad2.setZip((int)cell.getNumericCellValue());
							break;
						case 11:
							String cityName1 = cell.getStringCellValue();
							for(CityModel c : allCities){
								if(cityName1 != null && cityName1.equals(c.getCityName())){
									ad2.setCity((int)c.getCityId());
								}
							}
							break;
						}
						
					}
					addresses.add(ad1);
					addresses.add(ad2);
					eachEmpModel.setAddresses(addresses);
					employeeModelList.add(eachEmpModel);
				}
			}
			
			//System.out.println(employeeModelList);
			/*
			 * Modification for employee verification started
			 */
			//List<EmployeeModel> empList = validateEmployees(employeeModelList);
			
			Map<String, List<EmployeeModel>> empMap = validateEmployees(employeeModelList);
			List<EmployeeModel> passList = empMap.get("passList");
			List<EmployeeModel> failList = empMap.get("failList");
			redirect.addFlashAttribute("failList",failList);
			redirect.addFlashAttribute("departments",departments);
			redirect.addFlashAttribute("cities", allCities);
			
			//save Verified EmpList
			for(EmployeeModel e : passList){
				//validate each employee before saving. 
				employeeManagementService.saveEmployee(e);
			}
			
			/*
			 * Modification for employee verification ended
			 */
			
			/*
			for(EmployeeModel e : employeeModelList){
				//validate each employee before saving. 
				employeeManagementService.saveEmployee(e);
			}*/
			System.out.println(employeeModelList);
		} catch (IOException e) {
			e.printStackTrace();
			redirect.addFlashAttribute("err", "Something went wrong!");
			
		}
		
		return "redirect:/save-excel";
	}
	
	private Map<String,List<EmployeeModel>> validateEmployees(List<EmployeeModel> employees){
		//List<DepartmentModel> allDepartments = departmentService.getAllDepartments();
		List<EmployeeModel> passEmpList =new ArrayList<>();
		List<EmployeeModel> failEmpList =new ArrayList<>();
		Map<String,List<EmployeeModel>> empMap = new HashMap<>();
		
		for(EmployeeModel e : employees){
			StringJoiner errors = new StringJoiner(",");
			if(e.getEmployeeName().length() < 2){
				errors.add("Name should be atleast 2 characters");
			}
			if(e.getAge() < 1 || e.getAge() > 100){
				errors.add("Invalid age");
			}
			if(e.getSalary()<=0 || e.getSalary() >= 999999){
				errors.add("invalid salary");
			}
			if(e.getDepartment() == null){
				errors.add("Invalid department");
			}
			List<AddressModel> addresses = e.getAddresses();
			for(AddressModel a : addresses){
				if(a.getLine1().length() < 3){
					errors.add("address line1 is invalid");
				}
				if(a.getLine2().length() < 3){
					errors.add("address line2 is invalid");
				}
				if(a.getZip() < 1){
					errors.add("zip is invalid");
				}
				if(a.getCity() == null){
					errors.add("city is invalid");
				}
			}
			
			
			//adding to list
			if(errors.length() == 0){
				e.setStatus("SAVED");
				e.setErrors(errors.toString());
				passEmpList.add(e);
			}else{
				int length = errors.toString().split(",").length;
				if(length > 3){
					e.setStatus("FAILED");
				}else{
					e.setStatus("PARTIAL");
				}
				e.setErrors(errors.toString());
				failEmpList.add(e);
			}
			
		}
		empMap.put("passList", passEmpList);
		empMap.put("failList", failEmpList);
		return empMap;
	}
	
}

