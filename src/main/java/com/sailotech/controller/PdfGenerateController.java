package com.sailotech.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import com.sailotech.model.AddressModel;
import com.sailotech.model.CityModel;
import com.sailotech.model.DepartmentModel;
import com.sailotech.model.EmployeeModel;
import com.sailotech.service.CityManagementService;
import com.sailotech.service.DepartmentManagementService;
import com.sailotech.service.EmployeeManagementService;

@Controller
public class PdfGenerateController {
	private EmployeeManagementService empMgmtService;
	private DepartmentManagementService depService;
	private CityManagementService cityMgmt;
	
	
	@Autowired
	public PdfGenerateController(EmployeeManagementService empMgmtService,DepartmentManagementService depService,CityManagementService cityMgmt) {
		super();
		this.empMgmtService = empMgmtService;
		this.depService=depService;
		this.cityMgmt=cityMgmt;
	}



	@RequestMapping(path={"/generate-pdf/{eid}"},method={RequestMethod.GET})
	public ResponseEntity<?> generatePdfForId(@PathVariable("eid") Integer eid, Model model){
		EmployeeModel employee = empMgmtService.findEmployeeById(eid);
		model.addAttribute("employee", employee);
		//document preparation started
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Document doc = new Document();
		//doc.open();
		try {
			PdfWriter.getInstance(doc, out);
			List<DepartmentModel> allDep = depService.getAllDepartments();
			List<AddressModel> addresses = employee.getAddresses();
			//PdfTable table = new PdfTable(2);
			Table table = new Table(2);
			//table.setCellsFitPage(true);
			table.addCell("Name");
			table.addCell(employee.getEmployeeName());
			table.addCell("Age");
			table.addCell(String.valueOf(employee.getAge()));
			table.addCell("Salary");
			table.addCell(String.valueOf(employee.getSalary()));
			
			Table add1 = new Table(2);
			Table add2 = new Table(2);
			
			table.addCell("Department");
			Integer depId = employee.getDepartment();
			for(DepartmentModel dep : allDep){
				if(depId == dep.getDepartmentNo()){
					table.addCell(dep.getDepartmentName());
				}
			}
			table.addCell("Address 1");
			table.addCell("Address 2");
			
			
			
			
			List<CityModel> allCities = cityMgmt.getAllCities();
			int count=0;
			for(AddressModel a : addresses){
				if(count == 0){
					add1.addCell("Line1");
					add1.addCell(a.getLine1());
					add1.addCell("Line2");
					add1.addCell(a.getLine2());
					add1.addCell("Zip");
					add1.addCell(String.valueOf(a.getZip()));
					add1.addCell("city");
					Integer cityId = a.getCity();
					
					for(CityModel c : allCities){
						if(c.getCityId() == cityId){
							add1.addCell(c.getCityName());
						}
					}
					
				}else{
					add2.addCell("Line1");
					add2.addCell(a.getLine1());
					add2.addCell("Line2");
					add2.addCell(a.getLine2());
					add2.addCell("Zip");
					add2.addCell(String.valueOf(a.getZip()));
					add2.addCell("city");
					Integer cityId = a.getCity();
					for(CityModel c : allCities){
						if(c.getCityId() == cityId){
							add2.addCell(c.getCityName());
						}
					}
				}
				count++;
			}
			Cell cell1 = new Cell();
			cell1.add(add1);
			Cell cell2 = new Cell();
			cell2.add(add2);
			
			table.addCell(cell1);
			table.addCell(cell2);
			doc.open();
			doc.add(table);
			doc.close();
			out.close();
			byte[] docContent = out.toByteArray();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.setContentDispositionFormData("employee", "employee.pdf");
			ResponseEntity<byte[]> response = new ResponseEntity<>(docContent,headers,HttpStatus.OK);
			return response;
		} catch (DocumentException | IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error occured");
			e.printStackTrace();
		}
		
		ResponseEntity<?> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		//document prepration ended
		return response;
		//return "pdfView";
	}
}
