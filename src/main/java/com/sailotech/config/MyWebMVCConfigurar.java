package com.sailotech.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;
import org.springframework.web.servlet.view.XmlViewResolver;

@Configuration
public class MyWebMVCConfigurar implements WebMvcConfigurer{

	/*@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		
	}*/
	
	@Bean
	public ViewResolver internalResourceViewResolver(){
		InternalResourceViewResolver irvr = new InternalResourceViewResolver();
		irvr.setPrefix("/WEB-INF/jsp/");
		irvr.setSuffix(".jsp");
		irvr.setOrder(5);
		return irvr;
	}
	
	@Bean
	public ViewResolver beanNameViewResolver(){
		/*ResourceBundleViewResolver rbvr = new ResourceBundleViewResolver();
		rbvr.setBasename("view");
		rbvr.setOrder(1);*/
		//return rbvr;
		BeanNameViewResolver bnvr = new BeanNameViewResolver();
		bnvr.setOrder(1);
		return bnvr;
		
	}
	
}
