package com.sailotech.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {

	@Id
	@Column(name = "employee_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer employeeId;
	@Column(name = "employee_name")
	private String employeeName;
	@Column(name = "age", nullable = true)
	private Integer age;
	@Column(name = "salary", nullable = true)
	private Double salary;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "department_no")
	private Department department;

	@Column(name = "is_del_ind")
	// @ColumnDefault(value="n")
	private String isDelInd;

	@OneToMany(mappedBy = "employee")
	private Set<Address> address;

	@Column(name = "status")
	private String status;

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getIsDelInd() {
		return isDelInd;
	}

	public void setIsDelInd(String isDelInd) {
		this.isDelInd = isDelInd;
	}

	public Set<Address> getAddress() {
		return address;
	}

	public void setAddress(Set<Address> address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", age=" + age + ", salary="
				+ salary + ", department=" + department + ", isDelInd=" + isDelInd + ", address=" + address
				+ ", status=" + status + "]";
	}

}
