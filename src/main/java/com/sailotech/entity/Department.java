package com.sailotech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="department")
public class Department {
	
	@Id
	@Column(name="department_no")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int departmentNo;
	@Column(name="department_name")
	private String departmentName;
	@Column(name="is_del_ind")
	private String isDelInd;

	public int getDepartmentNo() {
		return departmentNo;
	}

	public void setDepartmentNo(int departmentNo) {
		this.departmentNo = departmentNo;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getIsDelInd() {
		return isDelInd;
	}

	public void setIsDelInd(String isDelInd) {
		this.isDelInd = isDelInd;
	}

	@Override
	public String toString() {
		return "Department [departmentNo=" + departmentNo + ", departmentName=" + departmentName + ", isDelInd="
				+ isDelInd + "]";
	}

	
}
