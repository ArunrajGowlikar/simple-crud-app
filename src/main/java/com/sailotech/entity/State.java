package com.sailotech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="state")
public class State {
	@Id
	@Column(name = "state_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer stateId;

	@Column(name = "state_name")
	private String stateName;
	@Column(name = "is_del_ind")
	private String isDelInd;
	
	
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getIsDelInd() {
		return isDelInd;
	}
	public void setIsDelInd(String isDelInd) {
		this.isDelInd = isDelInd;
	}
	@Override
	public String toString() {
		return "State [stateId=" + stateId + ", stateName=" + stateName + ", isDelInd=" + isDelInd + "]";
	}

	

}
