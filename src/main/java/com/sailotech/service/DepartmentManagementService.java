package com.sailotech.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sailotech.entity.Department;
import com.sailotech.model.DepartmentModel;
import com.sailotech.repository.DepartmentRepository;

@Service
public class DepartmentManagementService {
	
	private DepartmentRepository depRepo;

	@Autowired
	public DepartmentManagementService(DepartmentRepository depRepo) {
		super();
		this.depRepo = depRepo;
	}
	
	@Transactional(readOnly=true)
	public List<DepartmentModel> getAllDepartments(){
		List<DepartmentModel> departmentModelList = new ArrayList<>();
		List<Department> departments = depRepo.findAllUnDeletedDepartments();
		for (Department department : departments) {
			DepartmentModel depModel = new DepartmentModel();
			BeanUtils.copyProperties(department, depModel);
			departmentModelList.add(depModel);
		}
		return departmentModelList;
	}
	
	@Transactional(readOnly=true)
	public DepartmentModel getDepartmentById(int id){
		Department department = depRepo.findDepartmentById(id);
		DepartmentModel dep = new DepartmentModel();
		BeanUtils.copyProperties(department, dep);
		return dep;
	}
	
	
	
	
}
