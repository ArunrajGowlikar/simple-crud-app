package com.sailotech.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sailotech.entity.Address;
import com.sailotech.entity.City;
import com.sailotech.entity.Department;
import com.sailotech.entity.Employee;
import com.sailotech.model.AddressModel;
import com.sailotech.model.CityModel;
import com.sailotech.model.EmployeeModel;
import com.sailotech.repository.AddressRepository;
import com.sailotech.repository.DepartmentRepository;
import com.sailotech.repository.EmployeeRepository;

@Service
public class EmployeeManagementService {
	private EmployeeRepository empRepo;
	private DepartmentRepository depRepo;
	private CityManagementService cityManagementService;
	private AddressRepository addressRepo;
	
	@Autowired
	public EmployeeManagementService(EmployeeRepository empRepo, DepartmentRepository depRepo,CityManagementService cityManagementService,AddressRepository addressRepo) {
		super();
		this.empRepo = empRepo;
		this.depRepo = depRepo;
		this.cityManagementService = cityManagementService;
		this.addressRepo=addressRepo;
	}
	
	@Transactional(readOnly=false)
	public String saveEmployee(EmployeeModel empModel){
		StringBuilder sb = null;
		Employee emp = new Employee();
		BeanUtils.copyProperties(empModel, emp);
		

		//setting department
		int departmentId = empModel.getDepartment();
		Department department = depRepo.findDepartmentById(departmentId);
		emp.setDepartment(department);
		emp.setIsDelInd("n");
		
		/*
		 * get all address and conver to AddressEntity and attach to EmployeeEntity and save it
		 * */
		Set<Address> addressEntity = new HashSet<>();
		List<AddressModel> addresses = empModel.getAddresses();
		for(AddressModel addModel : addresses){
			Address address = new Address();
			address.setEmployee(emp);
			int cityId = addModel.getCity();
			City cityEntity = cityManagementService.getCityById(cityId);
			BeanUtils.copyProperties(addModel, address,"city");
			address.setCity(cityEntity);
			addressEntity.add(address);
		}
		//List<Address> saveAll = addressRepo.saveAll(addressEntity);
		/*Set<Address> setAddress = new HashSet<>();
		for(Address a : saveAll){
			setAddress.add(a);
		}*/
		
		/*modification end
		 * */
		if(emp.getEmployeeId() != 0){
			//update logic
			/*emp = empRepo.findByEmployeeIdAndIsDelIndIsNot(emp.getEmployeeId(),"n");*/
			/*emp = empRepo.findByEmployeeId(emp.getEmployeeId());*/
			//explicitly setting isDelInd=n bcoz it cannot be setted by @ColumnDefault or Default 'n' is schema
			
			//emp.setAddress(setAddress);
			
			//empRepo.save(emp);//working
			Employee save = empRepo.save(emp);
			//working
			Iterator<Address> itr = addressEntity.iterator();
			while(itr.hasNext()){
				Address a = itr.next();
				a.setEmployee(save);
				addressRepo.save(a);
			}
			sb = new StringBuilder("updated");
		}else{
			//insert logic
			//emp.setAddress(setAddress);
			
			Employee save = empRepo.save(emp);
			
			Iterator<Address> itr = addressEntity.iterator();
			while(itr.hasNext()){
				Address a = itr.next();
				a.setEmployee(save);
				addressRepo.save(a);
			}
			
			/*List<Address> saveAll = addressRepo.saveAll(addressEntity);
			Set<Address> setAddress = new HashSet<>();
			for(Address a : saveAll){
				setAddress.add(a);
			}*/
			sb = new StringBuilder("saved");
		}
		return sb.toString();
	}
	
	@Transactional(readOnly=true)
	public List<EmployeeModel> getAllEmployees(){
		/*List<Employee> allEmployees = empRepo.findAll();*/
		List<Employee> allEmployees = empRepo.findAllUnDeletedEmployees();
		List<EmployeeModel> allEmployeeModals = new ArrayList<>();
		for(Employee e : allEmployees){
			int depId = e.getDepartment().getDepartmentNo();
			EmployeeModel emp = new EmployeeModel();
			BeanUtils.copyProperties(e, emp, "department");
			emp.setDepartment(depId);
			allEmployeeModals.add(emp);
			//fetch all addresses of employee
			Set<Address> addresses = e.getAddress();
			Iterator<Address> itr = addresses.iterator();
			List<AddressModel> addressesModel = new ArrayList<>();
			while(itr.hasNext()){
				AddressModel model = new AddressModel();
				Address addEntity = itr.next();
				int cityId = addEntity.getCity().getCityId();
				model.setCity(cityId);
				BeanUtils.copyProperties(addEntity, model, "city","employee");
				addressesModel.add(model);
			}
			//setting AddressModel to EmployeeModel
			emp.setAddresses(addressesModel);
		}
		return allEmployeeModals;
	}
	
	@Transactional(readOnly=true)
	public EmployeeModel findEmployeeById(int id){
		Employee employee = empRepo.findByEmployeeId(id);
		/*Employee employee = empRepo.findByEmployeeIdAndIsDelIndIsNot(id,"n");*/
		/*get all Addresses associated with that emp and add to employeeModel
		 * */
		Set<Address> addressEntity = employee.getAddress();
		List<AddressModel> addressModel = new ArrayList<>();
		Iterator<Address> itr = addressEntity.iterator();
		while(itr.hasNext()){
			AddressModel model = new AddressModel();
			Address address = itr.next();
			int cityId = address.getCity().getCityId();
			BeanUtils.copyProperties(address, model,"city");
			model.setCity(cityId);
			addressModel.add(model);
		}
		int depId = employee.getDepartment().getDepartmentNo();
		EmployeeModel employeeModel = new EmployeeModel();
		BeanUtils.copyProperties(employee, employeeModel, "department");
		employeeModel.setDepartment(depId);
		employeeModel.setAddresses(addressModel);
		return employeeModel;
	}
	
	@Transactional(readOnly=false)
	public void deleteEmployeeById(int id){
		try{
			/*empRepo.deleteById(id);*/
			empRepo.deleteEmployee(id);
		}catch(Exception e){
			throw new IllegalArgumentException();
		}
	}
	
	@Transactional(readOnly=true)
	public List<EmployeeModel> getAllEmployeesByCityId(Integer cityId){
		List<EmployeeModel> employeeModal = null;
		List<CityModel> allCities = cityManagementService.getAllCities();
		employeeModal = addressRepo.findAllEmployeesByCityId(cityId).stream().map(e -> {
			EmployeeModel model = new EmployeeModel();
			model.setDepartment(e.getDepartment().getDepartmentNo());
			Set<Address> addresses = e.getAddress();
			List<AddressModel> addressList = new ArrayList<>();
			for(Address a : addresses){
				AddressModel amodel = new AddressModel();
				BeanUtils.copyProperties(a, amodel);
				amodel.setCity(a.getCity().getCityId());
				addressList.add(amodel);
			}
			model.setAddresses(addressList);
			BeanUtils.copyProperties(e, model);
			return model;
		}).collect(Collectors.toList());
		return employeeModal;
	}
	
}
