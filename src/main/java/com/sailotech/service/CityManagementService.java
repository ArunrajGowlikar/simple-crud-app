package com.sailotech.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sailotech.entity.City;
import com.sailotech.model.CityModel;
import com.sailotech.repository.CityRepository;

@Service
public class CityManagementService {
	private CityRepository cityRepo;

	@Autowired
	public CityManagementService(CityRepository cityRepo) {
		super();
		this.cityRepo = cityRepo;
	}
	
	@Transactional(readOnly=true)
	public List<CityModel> getAllCities(){
		List<CityModel> cities = null;
		List<City> cityEntity = cityRepo.findAll();
		cities = cityEntity.stream().map(s->{
			CityModel cityModel = new CityModel();
			BeanUtils.copyProperties(s, cityModel);
			return cityModel;
			}).collect(Collectors.toList());
		
		return cities;
	}
	
	@Transactional(readOnly=true)
	public City getCityById(Integer id){
		return cityRepo.findById(id).get();
	}
	
	
}
