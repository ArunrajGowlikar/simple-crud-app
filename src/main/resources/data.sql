insert into department (department_no,department_name) values (1,'Accounts');
insert into department (department_no,department_name) values (2,'Management');

--insert into employee (employee_id,employee_name,age,salary,department_no) 
--values(1,'susan',20,2000,1);
--insert into employee (employee_id,employee_name,age,salary,department_no) 
--values(2,'nancy',20,3000,2);

insert into state (state_id,state_name,is_del_ind) values(1,'Telangana','n');
insert into state (state_id,state_name,is_del_ind) values(2,'Karnataka','n');
insert into state (state_id,state_name,is_del_ind) values(3,'Chennai','n');

insert into city (city_id,city_name,state_id,is_del_ind) values(1,'Hyderabad',1,'n');
insert into city (city_id,city_name,state_id,is_del_ind) values(2,'Ranga Reddy',1,'n');
insert into city (city_id,city_name,state_id,is_del_ind) values(3,'Banglore',3,'n');

--insert into address (address_no,line1,line2,zip,city_id,employee_id) 
--values(1,'bank street','3rd left',1001,1,1);




