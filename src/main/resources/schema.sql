create table department 
(
department_no int constraint department_no_pk primary key, 
department_name varchar(25),
is_del_ind varchar(1) default 'n'
);

create table employee (
employee_id int constraint employee_id_pk primary key,
employee_name varchar(25),
age int,
salary double,
department_no int,
status varchar(25),
is_del_ind varchar(1) default 'n' not null,
constraint department_no_fk foreign key (department_no) references department (department_no)
);

create table state
(
state_id int,
state_name varchar(25),
is_del_ind varchar(1),
constraint state_id_pk primary key (state_id)
);

create table city
(
city_id int,
city_name varchar(25),
is_del_ind varchar(1),
state_id int,
constraint city_id_pk primary key (city_id),
constraint state_id_fk foreign key (state_id) references state (state_id)
);


create table address
(
address_no int,
line1 varchar(50),
line2 varchar(50),
zip int(5),
city_id int,
employee_id int,
constraint address_no_pk primary key (address_no),
constraint city_id_fk foreign key (city_id) references city (city_id),
constraint employee_id_fk foreign key (employee_id) references employee (employee_id)
);






